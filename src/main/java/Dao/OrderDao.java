/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao;

import Model.Orders;
import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author INGK
 */
public class OrderDao implements Dao<Orders>{

    @Override
    public Orders get(int id) {
        Orders item = null;
        String sql = "SELECT * FROM orders WHERE orders_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Orders.fromRS(rs);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }


    public List<Orders> getAll() {
        ArrayList<Orders> list = new ArrayList();
        String sql = "SELECT * FROM orders ";

        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Orders item = Orders.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    //@Override
    public List<Orders> getAll(String order) {
        ArrayList<Orders> list = new ArrayList();
        String sql = "SELECT * FROM orders ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Orders item = Orders.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    @Override
    public List<Orders> getAll(String where, String order) {
        ArrayList<Orders> list = new ArrayList();
        String sql = "SELECT * FROM orders where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Orders item = Orders.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }


    @Override
    public Orders save(Orders obj) {
        String sql = "INSERT INTO orders(branch_id,orders_item_id,emp_id,cus_id, orders_total,orders_discount,orders_cash,orders_change,orders_queue,orders_date,orders_time)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getBranch_id());
            stmt.setInt(2, obj.getOrders_item_id());
            stmt.setString(3, obj.getEmp_id());
            stmt.setInt(4, obj.getCus_id());
            stmt.setDouble(5, obj.getOrders_total());
            stmt.setString(6, obj.getOrders_discount());
            stmt.setString(7, obj.getOrders_cash());
            stmt.setString(8, obj.getOrders_change());
            stmt.setInt(9, obj.getOrders_queue());
            stmt.setString(10,obj.getOrders_date());
            stmt.setString(11,obj.getOrders_time());            
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setOrders_Id(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

     @Override
    public Orders update(Orders obj) {
       String sql = "UPDATE orders " + " SET orders_id = ?, branch_id = ?, orders_item_id = ?,emp_id = ?, cus_id = ?, orders_total = ?, orders_discount = ?, orders_cash = ?, order_change = ?,orders_queue = ?,orders_date = ?, order_time = ? "
                + " WHERE orders_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getOrders_Id());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Orders obj) {
        String sql = "DELETE FROM orders WHERE orders_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getOrders_Id());            
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
}
