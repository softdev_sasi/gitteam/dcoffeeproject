/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.Employee;
import Model.SalaryManagement;
import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class SalaryDao implements Dao<SalaryManagement> {

    @Override
    public SalaryManagement get(int id) {
        SalaryManagement item = null;
        String sql = "SELECT * FROM salary_report WHERE slr_report_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = SalaryManagement.fromrs(rs);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    //@Override
    public List<SalaryManagement> getAll() {
        ArrayList<SalaryManagement> list = new ArrayList();
        String sql = "SELECT  salary_report.slr_report_id, employee.emp_id, attendence_time.atd_time_id, attendence_time.atd_hours,salary_report.emp_salary, attendence_time.status_pay, salary_report.date_to_pay FROM salary_report\n"
                + "LEFT JOIN employee\n"
                + "ON salary_report.emp_id = employee.emp_id\n"
                + "LEFT JOIN attendence_time\n"
                + "ON salary_report.atd_time_id = attendence_time.atd_time_id \n"
                + "ORDER BY slr_report_id asc";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                SalaryManagement item = SalaryManagement.fromrs(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<SalaryManagement> getAll(String where, String order) {
        ArrayList<SalaryManagement> list = new ArrayList<SalaryManagement>();
        String sql = "SELECT * FROM salary_report where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                SalaryManagement item = SalaryManagement.fromrs(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<SalaryManagement> getAll(String order) {
        ArrayList<SalaryManagement> list = new ArrayList<SalaryManagement>();
        String sql = "SELECT * FROM salary_report  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                SalaryManagement item = SalaryManagement.fromrs(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public SalaryManagement save(SalaryManagement obj) {
        String sql = "INSERT INTO salary_report(emp_id, atd_time_id, atd_hours,date_to_pay, emp_salary)"
                + "VALUES(?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployeeid());
            stmt.setInt(2, obj.getTotalworking());
            stmt.setString(3, obj.getStatus());
            stmt.setInt(4, obj.getSalary());
            stmt.setString(5, obj.getDate());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public SalaryManagement update(SalaryManagement obj) {
        String sql = "UPDATE salary_report" + " SET emp_id = ?, atd_time_id = ?, atd_hours = ?,date_to_pay = ?, emp_salary = ? "
                + " WHERE slr_report_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployeeid());
            stmt.setInt(2, obj.getTotalworking());
            stmt.setString(3, obj.getStatus());
            stmt.setInt(4, obj.getSalary());
            stmt.setString(5, obj.getDate());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(SalaryManagement obj) {
        String sql = "DELETE FROM salary_report WHERE slr_report_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
