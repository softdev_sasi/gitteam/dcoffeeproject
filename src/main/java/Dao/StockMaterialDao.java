/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.StockMaterial;
import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author admin
 */
public class StockMaterialDao implements Dao<StockMaterial>{
    @Override
    public StockMaterial get(int id) {
        StockMaterial item = null;
        String sql = "SELECT * FROM stock_material WHERE stock_mtr_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = StockMaterial.fromRS(rs);
                
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    //@Override
    public List<StockMaterial> getAll() {
        ArrayList<StockMaterial> list = new ArrayList();
        String sql = "SELECT * FROM stock_material";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                StockMaterial item = StockMaterial.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<StockMaterial> getAll(String where, String order) {
        ArrayList<StockMaterial> list = new ArrayList();
        String sql = "SELECT * FROM stock_material where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                StockMaterial item = StockMaterial.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<StockMaterial> getAll(String order) {
        ArrayList<StockMaterial> list = new ArrayList();
        String sql = "SELECT * FROM stock_material  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                StockMaterial item = StockMaterial.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public StockMaterial save(StockMaterial obj) {
        String sql = "INSERT INTO stock_material (stock_mtr_name, stock_mtr_minimum, stock_mtr_remaining, stock_mtr_unit)" 
                            + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getMinimum());
            stmt.setInt(3, obj.getRemaining());
            stmt.setString(4, obj.getUnit());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public StockMaterial update(StockMaterial obj) {
        String sql = "UPDATE stock_material" + " SET stock_mtr_name = ?, stock_mtr_minimum = ?, stock_mtr_remaining = ?, stock_mtr_unit = ?"
                            + " WHERE stock_mtr_id = ?";
        Connection conn = DatabaseHelper.getConnect();
         try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getMinimum());
            stmt.setInt(3, obj.getRemaining());
            stmt.setString(4, obj.getUnit());
            stmt.setInt(5, obj.getId());
            int ret = stmt.executeUpdate();
             System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    
    }

    @Override
    public int delete(StockMaterial obj) {
        String sql = "DELETE FROM stock_material WHERE stock_mtr_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

}
