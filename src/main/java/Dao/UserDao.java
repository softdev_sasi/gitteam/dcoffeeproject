/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.UserManagement;
import Model.UserManagement;
import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author USER
 */
public class UserDao implements Dao<UserManagement>{
    @Override
    public  UserManagement get(int id) {
        UserManagement item = null;
        String sql = "SELECT * FROM user WHERE user_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = UserManagement.fromRS(rs);
                
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    //@Override
    public List<UserManagement> getAll() {
        ArrayList<UserManagement> list = new ArrayList();
        String sql = "SELECT * FROM user";
        Connection conn = null;
        try {
            conn = DatabaseHelper.getConnect();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                UserManagement item = UserManagement.fromRS(rs);
                list.add(item);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }
    
    @Override
    public List<UserManagement> getAll(String where, String order) {
        ArrayList<UserManagement> list = new ArrayList<UserManagement>();
        String sql = "SELECT * FROM user where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                UserManagement item = UserManagement.fromRS(rs);
                list.add(item);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<UserManagement> getAll(String order) {
        ArrayList<UserManagement> list = new ArrayList<UserManagement>();
        String sql = "SELECT * FROM user  ORDER BY" + order;
        Connection conn = null;
        try {
            conn = DatabaseHelper.getConnect();

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                UserManagement item = UserManagement.fromRS(rs);
                list.add(item);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        DatabaseHelper.close();
        return list;
    }

    @Override
    public UserManagement save(UserManagement obj) {
        String sql = "INSERT INTO user (emp_username, emp_password,atd_date,atd_time_in,atd_time_out)" 
                            + "VALUES(?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            
            stmt.setString(1, obj.getUsername());
            stmt.setString(2, obj.getPassword());
            stmt.setString(3, obj.getDate());
            stmt.setString(4, obj.getTimein());
            stmt.setString(5,obj.getTimeout());
            
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public UserManagement update(UserManagement obj) {
        String sql = "UPDATE user" + " emp_username = ?, emp_password = ?,atd_date =? , atd_time_in = ?,atd_time_out = ?"
                            + " WHERE user_id = ?";
        Connection conn = DatabaseHelper.getConnect();
         try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getUsername());
            stmt.setString(2, obj.getPassword());
            stmt.setString(3, obj.getDate());
            stmt.setString(4, obj.getTimein());
            stmt.setString(5, obj.getTimeout());
            stmt.setInt(6, obj.getId());
            int ret = stmt.executeUpdate();
             System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    
    }

    @Override
    public int delete(UserManagement obj) {
        String sql = "DELETE FROM user WHERE user_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }
    
}

