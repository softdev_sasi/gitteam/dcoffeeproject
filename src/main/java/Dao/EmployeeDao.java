/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.Employee;
import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Thanya
 */
public class EmployeeDao implements Dao<Employee> {

    @Override
    public Employee get(int id) {
        Employee item = null;
        String sql = "SELECT * FROM employee WHERE emp_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Employee.fromRS(rs);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    //@Override
    public List<Employee> getAll() {
        ArrayList<Employee> list = new ArrayList();
        String sql = "SELECT  employee.emp_id, branch.branch_id, employee.emp_username, employee.emp_password,employee.emp_position,\n"
                + "employee.emp_firstname,employee.emp_lastname,employee.emp_age,employee.emp_gender,employee.emp_phone,employee.emp_email,\n"
                + "employee.emp_address,employee.emp_salary FROM employee\n"
                + "LEFT JOIN branch\n"
                + "ON employee.branch_id = branch.branch_id";
        Connection conn = null;
        try {
            conn = DatabaseHelper.getConnect();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee item = Employee.fromRS(rs);
                list.add(item);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }

    @Override
    public List<Employee> getAll(String where, String order) {
        ArrayList<Employee> list = new ArrayList<Employee>();
        String sql = "SELECT * FROM employee where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee item = Employee.fromRS(rs);
                list.add(item);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Employee> getAll(String order) {
        ArrayList<Employee> list = new ArrayList<Employee>();
        String sql = "SELECT * FROM employee  ORDER BY" + order;
        Connection conn = null;
        try {
            conn = DatabaseHelper.getConnect();

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee item = Employee.fromRS(rs);
                list.add(item);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        DatabaseHelper.close();
        return list;
    }
    
    public Employee getByLogin(String userName, String password, String position) {
        Employee emp = null;
        String sql = "SELECT * FROM employee WHERE emp_username=?, emp_password=?, emp_position=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, userName);
            stmt.setString(2, password);
            stmt.setString(3, position);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                emp = Employee.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return emp;
    }

    @Override
    public Employee save(Employee obj) {
        String sql = "INSERT INTO employee (branch_id, emp_username, emp_password, emp_position, emp_firstname, emp_lastname, emp_age, emp_gender, emp_phone, emp_email, emp_address, emp_salary)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getBranchId());
            stmt.setString(2, obj.getUsername());
            stmt.setString(3, obj.getPassword());
            stmt.setString(4, obj.getPosition());
            stmt.setString(5, obj.getFirstname());
            stmt.setString(6, obj.getLastname());
            stmt.setInt(7, obj.getAge());
            stmt.setString(8, obj.getGender());
            stmt.setString(9, obj.getPhone());
            stmt.setString(10, obj.getEmail());
            stmt.setString(11, obj.getAddress());
            stmt.setInt(12, obj.getSalary());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Employee update(Employee obj) {
        String sql = "UPDATE employee" + " SET branch_id = ?, emp_username = ?, emp_password = ?, emp_position = ?, emp_firstname = ?, emp_lastname = ?, emp_age = ?, emp_gender = ?, emp_phone = ?, emp_email = ?, emp_address = ?, emp_salary = ?"
                + " WHERE emp_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getBranchId());
            stmt.setString(2, obj.getUsername());
            stmt.setString(3, obj.getPassword());
            stmt.setString(4, obj.getPosition());
            stmt.setString(5, obj.getFirstname());
            stmt.setString(6, obj.getLastname());
            stmt.setInt(7, obj.getAge());
            stmt.setString(8, obj.getGender());
            stmt.setString(9, obj.getPhone());
            stmt.setString(10, obj.getEmail());
            stmt.setString(11, obj.getAddress());
            stmt.setInt(12, obj.getSalary());
            stmt.setInt(13, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }

    }

    @Override
    public int delete(Employee obj) {
        String sql = "DELETE FROM employee WHERE emp_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
