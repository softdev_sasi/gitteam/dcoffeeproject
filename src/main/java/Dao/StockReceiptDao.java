/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.StockReceipt;
import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author admin
 */
public class StockReceiptDao implements Dao<StockReceipt> {
    @Override
    public StockReceipt get(int id) {
        StockReceipt item = null;
        String sql = "SELECT * FROM receipt_item WHERE receipt_item_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = StockReceipt.fromRS(rs);
                
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    //@Override
    public List<StockReceipt> getAll() {
        ArrayList<StockReceipt> list = new ArrayList();
        String sql = "SELECT * FROM receipt_item";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                StockReceipt item = StockReceipt.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<StockReceipt> getStockMrt() {
        ArrayList<StockReceipt> list = new ArrayList();
        String sql = "SELECT stock_material.stock_mtr_id, stock_material.stock_mtr_name FROM receipt_item\n"
                            + "LEFT JOIN stock_material\n"
                            + "ON receipt_itm.stock_mtr_id = stock_material.stock_mtr_id";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                StockReceipt item = StockReceipt.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<StockReceipt> getAll(String where, String order) {
        ArrayList<StockReceipt> list = new ArrayList();
        String sql = "SELECT * FROM receipt_item where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                StockReceipt item = StockReceipt.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<StockReceipt> getAll(String order) {
        ArrayList<StockReceipt> list = new ArrayList();
        String sql = "SELECT * FROM receipt_item  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                StockReceipt item = StockReceipt.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public StockReceipt save(StockReceipt obj) {
        String sql = "INSERT INTO receipt_item (stock_mtr_name, receipt_item_quantity, receipt_item_total_price)" 
                            + "VALUES(?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getQuantity());
            stmt.setDouble(3, obj.getPrice());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public StockReceipt update(StockReceipt obj) {
        String sql = "UPDATE receipt_item" + " SET receipt_item_quantity = ?, receipt_item_total_price = ?"
                            + " WHERE receipt_item_id = ?";
        Connection conn = DatabaseHelper.getConnect();
         try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getQuantity());
            stmt.setDouble(2, obj.getPrice());
            stmt.setInt(3, obj.getId());
            int ret = stmt.executeUpdate();
             System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    
    }

    @Override
    public int delete(StockReceipt obj) {
        String sql = "DELETE FROM receipt_item WHERE receipt_item_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

}
