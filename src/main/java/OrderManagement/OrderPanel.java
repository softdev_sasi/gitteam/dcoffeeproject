/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OrderManagement;

import Model.Orders;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import service.OrderService;

/**
 *
 * @author admin
 */
public class OrderPanel extends javax.swing.JPanel {
    private final OrderService OrdService;
    private List<Orders> list;
    private Orders editedOrd;
    /**
     * Creates new form OrderPanel
     */
    public OrderPanel() {
        initComponents();
        OrdService = new OrderService();
        list = OrdService.getOrder();
        tblOrder.setModel(new AbstractTableModel() {
            String[] columnNames  = {"orders_id","Branch_Id", "orders_item_id", "emp_id", "cus_id", "orders_total", "orders_discoint", "orders_cash", "orders_change_", "orders_queue", "orders_date", "orders_time"};
            
            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }
            
            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return 12;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Orders ord = list.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return ord.getOrders_Id();
                    case 1:
                        return ord.getBranch_id();
                    case 2:
                        return ord.getOrders_item_id();
                    case 3:
                        return ord.getEmp_id();
                    case 4:
                        return ord.getCus_id();
                    case 5:
                        return ord.getOrders_total();
                    case 6:
                        return ord.getOrders_discount();
                    case 7:
                        return ord.getOrders_cash();
                    case 8:
                        return ord.getOrders_change();
                    case 9:
                        return ord.getOrders_queue();
                    case 10:
                        return ord.getOrders_date();
                    case 11:
                        return ord.getOrders_time();
                    default:
                        return "Unknowm";
                }
            }
            
        });
        enableForm(false);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        edtOrders_time = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblOrder = new javax.swing.JTable();
        edtOrders_date = new javax.swing.JTextField();
        edtOrders_queue = new javax.swing.JTextField();
        edtOrders_change = new javax.swing.JTextField();
        edtOrders_cash = new javax.swing.JTextField();
        edtOrders_discount = new javax.swing.JTextField();
        edtOrders_total = new javax.swing.JTextField();
        edtCus_id = new javax.swing.JTextField();
        edtEmp_id = new javax.swing.JTextField();
        edtOrders_item_id = new javax.swing.JTextField();
        edtBranch_id = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();

        btnEdit.setText("Edit");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        edtOrders_time.setText("time");
        edtOrders_time.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtOrders_timeActionPerformed(evt);
            }
        });

        tblOrder.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5", "Title 6", "Title 7", "Title 8", "Title 9", "Title 10", "Title 11"
            }
        ));
        jScrollPane1.setViewportView(tblOrder);

        edtOrders_date.setText("date");

        edtOrders_queue.setText("queue");
        edtOrders_queue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtOrders_queueActionPerformed(evt);
            }
        });

        edtOrders_change.setText("change");

        edtOrders_cash.setText("cash");

        edtOrders_discount.setText("discount");

        edtOrders_total.setText("total");

        edtCus_id.setText("cus");

        edtEmp_id.setText("emp");

        edtOrders_item_id.setText("item");

        edtBranch_id.setText("branch");

        jLabel1.setText("branch :");

        jLabel2.setText("item :");

        jLabel3.setText("employee :");

        jLabel4.setText("customer :");

        jLabel5.setText("total :");

        jLabel6.setText("discount :");

        jLabel7.setText("cash :");

        jLabel8.setText("Change :");

        jLabel9.setText("queue :");

        jLabel10.setText("date :");

        jLabel11.setText("time :");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(edtCus_id, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel8))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(edtEmp_id, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel7))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(edtOrders_item_id, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(edtBranch_id, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(40, 40, 40)
                                        .addComponent(jLabel5))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel6)))))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(edtOrders_cash, javax.swing.GroupLayout.DEFAULT_SIZE, 122, Short.MAX_VALUE)
                            .addComponent(edtOrders_discount)
                            .addComponent(edtOrders_total)
                            .addComponent(edtOrders_change))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel9)
                            .addComponent(jLabel10)
                            .addComponent(jLabel11))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(0, 113, Short.MAX_VALUE)
                                .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(edtOrders_date, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(edtOrders_queue, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(edtOrders_time, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnDelete)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnEdit)
                        .addComponent(jLabel1)
                        .addComponent(edtBranch_id, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel5)
                        .addComponent(edtOrders_total, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel9)
                    .addComponent(edtOrders_queue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edtOrders_item_id, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(edtOrders_discount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel7)
                    .addComponent(jLabel10)
                    .addComponent(edtOrders_date, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edtOrders_cash, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edtEmp_id, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel8)
                    .addComponent(jLabel11)
                    .addComponent(edtOrders_time, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edtCus_id, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edtOrders_change, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 349, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
               int selectedIndex = tblOrder.getSelectedRow();
               if(selectedIndex >= 0) {
                   editedOrd = list.get(selectedIndex);
                   setObjectToForm();
                   enableForm(true);
                   System.out.println("ddddddddddddddddd");
               }   
    }//GEN-LAST:event_btnEditActionPerformed
    private void refreshTable() {
        list = OrderService.getOrder();
        tblOrder.revalidate();
        tblOrder.repaint();
    }
    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
         int selectedIndex = tblOrder.getSelectedRow();
               if(selectedIndex >= 0) {
                   editedOrd = list.get(selectedIndex);
                   int input = JOptionPane.showConfirmDialog(this, "Do you want to delete?", "Select an Option...", JOptionPane.YES_NO_OPTION,JOptionPane.ERROR_MESSAGE);
                   if(input == 0) {
                       OrderService.delete(editedOrd);
                   }
                   refreshTable();
               } // TODO add your handling code here:
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void edtOrders_timeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtOrders_timeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtOrders_timeActionPerformed

    private void edtOrders_queueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtOrders_queueActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtOrders_queueActionPerformed
    private void setObjectToForm() {
            edtBranch_id.setText("" + editedOrd.getBranch_id()+"");
            edtOrders_item_id.setText("" + editedOrd.getOrders_item_id() + "");
            edtEmp_id.setText(editedOrd.getEmp_id());
            edtCus_id.setText("" + editedOrd.getCus_id());
            edtOrders_total.setText("" + editedOrd.getOrders_total());
            edtOrders_discount.setText(editedOrd.getOrders_discount());
            edtOrders_cash.setText(editedOrd.getOrders_cash());
            edtOrders_change.setText(editedOrd.getOrders_change());
            edtOrders_queue.setText(""+ editedOrd.getOrders_queue());
            edtOrders_date.setText("" + editedOrd.getOrders_date());
            edtOrders_time.setText("" + editedOrd.getOrders_time());
        }
    
    private void setFormToObject() {
        editedOrd.setBranch_id(Integer.parseInt(edtBranch_id.getText()));
        editedOrd.setOrders_item_id(Integer.parseInt(edtOrders_item_id.getText()));
        editedOrd.setEmp_id(edtEmp_id.getText());
        editedOrd.setCus_id(Integer.parseInt(edtCus_id.getText()));
        editedOrd.setOrders_total(Integer.parseInt(edtOrders_total.getText()));
        editedOrd.setOrders_discount(edtOrders_discount.getText());
        editedOrd.setOrders_cash(edtOrders_cash.getText());
        editedOrd.setOrders_change(edtOrders_change.getText());
        editedOrd.setOrders_queue(Integer.parseInt(edtOrders_queue.getText()));
        //editedOrd.setOrders_date(edtOrders_date.getText());
        //editedOrd.setOrders_time(edtOrders_time.getText());             
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JTextField edtBranch_id;
    private javax.swing.JTextField edtCus_id;
    private javax.swing.JTextField edtEmp_id;
    private javax.swing.JTextField edtOrders_cash;
    private javax.swing.JTextField edtOrders_change;
    private javax.swing.JTextField edtOrders_date;
    private javax.swing.JTextField edtOrders_discount;
    private javax.swing.JTextField edtOrders_item_id;
    private javax.swing.JTextField edtOrders_queue;
    private javax.swing.JTextField edtOrders_time;
    private javax.swing.JTextField edtOrders_total;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblOrder;
    // End of variables declaration//GEN-END:variables
    private void enableForm(boolean status) {
        if (status == false) {
            edtBranch_id.setText("");
            edtOrders_item_id.setText("");
            edtEmp_id.setText("");
            edtCus_id.setText("");
            edtOrders_total.setText("");
            edtOrders_discount.setText("");
            edtOrders_cash.setText("");
            edtOrders_change.setText("");
            edtOrders_queue.setText("");
            edtOrders_date.setText("");
            edtOrders_time.setText("");
        }
            edtBranch_id.setEnabled(status);
            edtOrders_item_id.setEnabled(status);
            edtEmp_id.setEnabled(status);
            edtCus_id.setEnabled(status);
            edtOrders_total.setEnabled(status);
            edtOrders_discount.setEnabled(status);
            edtOrders_cash.setEnabled(status);
            edtOrders_change.setEnabled(status);
            edtOrders_queue.setEnabled(status);
            edtOrders_date.setEnabled(status);
            edtOrders_time.setEnabled(status);
    }

}

