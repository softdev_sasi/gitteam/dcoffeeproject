/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import Dao.ProductDao;
import Model.Product;
import java.util.List;

/**
 *
 * @author acer
 */
public class ProductService {

    public List<Product> getID() {
        ProductDao pdtDao = new ProductDao();
        return pdtDao.getAll(" product_id asc");
    }

    public List<Product> getAll() {
        ProductDao pdtDao = new ProductDao();
        return pdtDao.getAll();
    }

    public Product addNew(Product editedEmp) {
        ProductDao dtDao = new ProductDao();
        return dtDao.save(editedEmp);
    }

    public Product update(Product editedProduct) {
        ProductDao dtDao = new ProductDao();
        return dtDao.update(editedProduct);
    }

    public static int delete(Product editedProduct) {
        ProductDao dtDao = new ProductDao();
        return dtDao.delete(editedProduct);
    }

}
