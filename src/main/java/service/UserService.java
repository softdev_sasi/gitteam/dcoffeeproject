/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;
import Dao.UserDao;
import Model.UserManagement;
import java.util.List;
/**
 *
 * @author USER
 */
public class UserService {
    public List<UserManagement> getUserManagements() {
        UserDao userDao = new UserDao();
        return userDao.getAll(" user_id asc");
    }

    public UserManagement addNew(UserManagement editedUser) {
        UserDao userDao = new UserDao();
        return userDao.save(editedUser);
    }

    public UserManagement update(UserManagement editedUser) {
        UserDao userDao = new UserDao();
        return userDao.update(editedUser);
    }
    
    public static int delete(UserManagement editedUser) {
        UserDao empDao = new UserDao();
        return empDao.delete(editedUser);
    }
}

    

