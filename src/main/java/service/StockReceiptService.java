/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import Dao.StockReceiptDao;
import Model.StockReceipt;
import java.util.List;

/**
 *
 * @author Thanya
 */
public class StockReceiptService {
    private StockReceipt editedStockReceipt;
    
    public List<StockReceipt> getStockReceipts() {
        StockReceiptDao stockReceiptDao = new StockReceiptDao();
        return stockReceiptDao.getAll(" receipt_item_id asc");
    }
    
    public StockReceipt addNew(StockReceipt editedStockReceipt) {
        StockReceiptDao stockReceiptDao = new StockReceiptDao();
        return stockReceiptDao.save(editedStockReceipt);
    }

    public StockReceipt update(StockReceipt editedStockReceipt) {
        StockReceiptDao stockReceiptDao = new StockReceiptDao();
        return stockReceiptDao.update(editedStockReceipt);
    }

    public int delete(StockReceipt editedStockReceipt) {
        StockReceiptDao stockReceiptDao = new StockReceiptDao();
        return stockReceiptDao.delete(editedStockReceipt);
    }
}
