/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import Dao.EmployeeDao;
import Model.Employee;
import java.util.List;

/**
 *
 * @author Thanya
 */
public class EmployeeService {
    
    public List<Employee> getEmployees() {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.getAll(" emp_id asc");
    }

    public Employee addNew(Employee editedEmp) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.save(editedEmp);
    }

    public Employee update(Employee editedEmp) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.update(editedEmp);
    }
    
    public static int delete(Employee editedEmp) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.delete(editedEmp);
    }
    
    public Employee login(String userName, String password, String position) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.getByLogin(userName, password, position);
    }
    
}
