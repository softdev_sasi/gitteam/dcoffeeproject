/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import Dao.CustomerDao;
import Model.Customer;
import java.util.List;

/**
 *
 * @author Thanya
 */
public class CustomerService {
    public List<Customer> getCustomers() {
        CustomerDao cusDao = new CustomerDao();
        return cusDao.getAll(" cus_id asc");
    }
    
    public Customer addNew(Customer editedCustomer) {
        CustomerDao cusDao = new CustomerDao();
        return cusDao.save(editedCustomer);
    }

    public Customer update(Customer editedCustomer) {
        CustomerDao cusDao = new CustomerDao();
        return cusDao.update(editedCustomer);
    }

    public int delete(Customer editedCustomer) {
        CustomerDao cusDao = new CustomerDao();
        return cusDao.delete(editedCustomer);
    }
}
