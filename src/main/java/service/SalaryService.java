/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import Dao.SalaryDao;
import Model.SalaryManagement;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class SalaryService {
    public List<SalaryManagement> getSalary(){
        SalaryDao slrDao = new SalaryDao();
        return slrDao.getAll();
    }
    
    public SalaryManagement addNew(SalaryManagement editedslr) {
        SalaryDao slrDao = new SalaryDao();
        return slrDao.save(editedslr);
    }

    public SalaryManagement update(SalaryManagement editedslr) {
        SalaryDao slrDao = new SalaryDao();
        return slrDao.update(editedslr);
    }
    
    public static int delete(SalaryManagement editedslr) {
        SalaryDao slrDao = new SalaryDao();
        return slrDao.delete(editedslr);
    }
}
