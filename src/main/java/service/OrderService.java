/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import Dao.OrderDao;
import Model.Orders;
import java.util.List;

/**
 *
 * @author INGK
 */
public class OrderService {



    public static List<Orders> getOrder(){
        OrderDao OrdDao = new OrderDao();
        return OrdDao.getAll(" orders_id asc");
    }
    
    public Orders addNew(Orders editedOrd) {
       OrderDao OrderDao = new OrderDao();
        return OrderDao.save(editedOrd);
    }

    public Orders update(Orders editedOrd) {
        OrderDao OrderDao = new OrderDao();
        return OrderDao.update(editedOrd);
    }
    
    public static int delete(Orders editedOrd) {
        OrderDao OrderDao = new OrderDao();
        return OrderDao.delete(editedOrd);
    }
}     
  

