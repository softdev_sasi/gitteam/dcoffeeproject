/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import Dao.StockMaterialDao;
import Model.StockMaterial;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author admin
 */
public class StockMaterialService {
       public List<StockMaterial> getStockMaterials() {
        StockMaterialDao stockMtrDao = new StockMaterialDao();
        return stockMtrDao.getAll(" stock_mtr_id asc");
    }

    public StockMaterial addNew(StockMaterial editedStockMaterial) {
        StockMaterialDao stockMtrDao = new StockMaterialDao();
        return stockMtrDao.save(editedStockMaterial);
    }

    public StockMaterial update(StockMaterial editedStockMaterial) {
        StockMaterialDao stockMtrDao = new StockMaterialDao();
        return stockMtrDao.update(editedStockMaterial);
    }

    public int delete(StockMaterial editedStockMaterial) {
        StockMaterialDao stockMtrDao = new StockMaterialDao();
        return stockMtrDao.delete(editedStockMaterial);
    }
}
