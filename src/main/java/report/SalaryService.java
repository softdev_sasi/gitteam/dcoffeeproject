/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import java.util.List;

/**
 *
 * @author admin
 */
public class SalaryService {
    public List<Salary> getReportSalaryByMonth(String year) {
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.getMonthReport(year);
    }
    
    public List<Salary> getReportSalaryByMonth() {
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.getMonthReport();
    }
    
    public List<Salary> getReportSalaryByDay(String month) {
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.getDayReport(month);
    }
    
    public List<Salary> getReportSalaryByDay() {
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.getDayReport();
    }
}
