/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author admin
 */
public class AddStockDao {

    public List<AddStock> getMonthReport(String year) {
        ArrayList<AddStock> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m\", receipt_date) as period, SUM(receipt_total_price) as Total FROM receipt_report\n"
                + "WHERE strftime(\"%Y\", receipt_date) = \""+year+"\"\n"
                + "GROUP BY period\n"
                + "ORDER BY period DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                AddStock item = AddStock.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<AddStock> getMonthReport() {
        ArrayList<AddStock> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m\", receipt_date) as period, SUM(receipt_total_price) as Total FROM receipt_report\n"
                + "GROUP BY period\n"
                + "ORDER BY period DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                AddStock item = AddStock.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<AddStock> getDayReport(String month) {
        ArrayList<AddStock> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m-%d\", receipt_date) as period, SUM(receipt_total_price) as Total FROM receipt_report\n"
                + "WHERE strftime(\"%m\", receipt_date) = \""+month+"\"\n"
                + "GROUP BY period\n"
                + "ORDER BY period DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                AddStock item = AddStock.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<AddStock> getDayReport() {
        ArrayList<AddStock> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m-%d\", receipt_date) as period, SUM(receipt_total_price) as Total FROM receipt_report\n"
                +  "GROUP BY period\n"
                + "ORDER BY period DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                AddStock item = AddStock.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
