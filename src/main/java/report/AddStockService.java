/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import java.util.List;

/**
 *
 * @author admin
 */
public class AddStockService {
    public List<AddStock> getReportAddStockByMonth(String year) {
        AddStockDao AddStockDao = new AddStockDao();
        return AddStockDao.getMonthReport(year);
    }
    
    public List<AddStock> getReportAddStockByDay(String month) {
        AddStockDao AddStockDao = new AddStockDao();
        return AddStockDao.getDayReport(month);
    }
    
    public List<AddStock> getReportAddStockByDay( ) {
        AddStockDao AddStockDao = new AddStockDao();
        return AddStockDao.getDayReport();
    }
    
    public List<AddStock> getReportAddStockByMonth() {
        AddStockDao AddStockDao = new AddStockDao();
        return AddStockDao.getMonthReport();
    }
}
