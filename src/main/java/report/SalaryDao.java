/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author admin
 */
public class SalaryDao {

    public List<Salary> getMonthReport() {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m\", date_to_pay) as period, SUM(emp_salary) as Total FROM salary_report\n"
                + "GROUP BY period\n"
                + "ORDER BY period DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Salary item = Salary.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<Salary> getMonthReport(String year) {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m\", date_to_pay) as period, SUM(emp_salary) as Total FROM salary_report\n"
                + "WHERE strftime(\"%Y\", date_to_pay) = \"" + year + "\"\n"
                + "GROUP BY period\n"
                + "ORDER BY period DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Salary item = Salary.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Salary> getDayReport(String month) {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m-%d\", date_to_pay) as period, SUM(emp_salary) as Total FROM salary_report\n"
                + "WHERE strftime(\"%m\", date_to_pay) = \"" + month + "\"\n"
                + "GROUP BY period\n"
                + "ORDER BY period DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Salary item = Salary.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    public List<Salary> getDayReport() {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m-%d\", date_to_pay) as period, SUM(emp_salary) as Total FROM salary_report\n"
                + "GROUP BY period\n"
                + "ORDER BY period DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Salary item = Salary.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
