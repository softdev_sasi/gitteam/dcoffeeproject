/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author admin
 */
public class AddStock {
    String period;
    double total;

    public AddStock(String period, double total) {
        this.period = period;
        this.total = total;
    }

    public AddStock() {
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "AddStock{" + "period=" + period + ", total=" + total + '}';
    }
    
    public static AddStock fromRS(ResultSet rs) {
        AddStock obj = new AddStock();
        try {
            obj.setPeriod(rs.getString("period"));
            obj.setTotal(rs.getDouble("total"));
        } catch (SQLException ex) {
            Logger.getLogger(Sale.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }
}
