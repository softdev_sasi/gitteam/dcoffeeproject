/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author admin
 */
public class SaleDao {

    public List<Sale> getMonthReport(String year) {
        ArrayList<Sale> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m\", orders_date) as period, SUM(orders_total) as Total FROM orders\n"
                + "LEFT JOIN orders_item\n"
                + "ON orders.orders_id = orders_item.orders_item_id\n"
                + "WHERE strftime(\"%Y\", orders_date) =\""+year+"\"\n"
                + "GROUP BY period\n"
                + "ORDER BY period DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Sale item = Sale.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Sale> getMonthReport() {
        ArrayList<Sale> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m\", orders_date) as period, SUM(orders_total) as Total FROM orders\n"
                + "LEFT JOIN orders_item\n"
                + "ON orders.orders_id = orders_item.orders_item_id\n"
                + "GROUP BY period\n"
                + "ORDER BY period DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Sale item = Sale.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<Sale> getDayReport(String month) {
        ArrayList<Sale> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m-%d\", orders_date) as period, SUM(orders_total) as Total FROM orders\n"
                + "LEFT JOIN orders_item\n"
                + "ON orders.orders_id = orders_item.orders_item_id\n"
                + "WHERE strftime(\"%m\", orders_date) =\""+month+"\"\n"
                + "GROUP BY period\n"
                + "ORDER BY period DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Sale item = Sale.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<Sale> getDayReport() {
        ArrayList<Sale> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m-%d\", orders_date) as period, SUM(orders_total) as Total FROM orders\n"
                + "LEFT JOIN orders_item\n"
                + "ON orders.orders_id = orders_item.orders_item_id\n"
                + "GROUP BY period\n"
                + "ORDER BY period DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Sale item = Sale.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
