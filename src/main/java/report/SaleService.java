/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import java.util.List;

/**
 *
 * @author admin
 */
public class SaleService {
    public List<Sale> getReportSaleByMonth(String year) {
        SaleDao SaleDao = new SaleDao();
        return SaleDao.getMonthReport(year);
    }
    
    public List<Sale> getReportSaleByDay(String month) {
        SaleDao SaleDao = new SaleDao();
        return SaleDao.getDayReport(month);
    }
    
    public List<Sale> getReportSaleByDay() {
        SaleDao SaleDao = new SaleDao();
        return SaleDao.getDayReport();
    }
    
    public List<Sale> getReportSaleByMonth() {
        SaleDao SaleDao = new SaleDao();
        return SaleDao.getMonthReport();
    }
}
