/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author admin
 */
public class Salary {
    String period;
    double total;

    public Salary(String period, double total) {
        this.period = period;
        this.total = total;
    }

    public Salary() {
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "Salary{" + "period=" + period + ", total=" + total + '}';
    }
    
    public static Salary fromRS(ResultSet rs) {
        Salary obj = new Salary();
        try {
            obj.setPeriod(rs.getString("period"));
            obj.setTotal(rs.getDouble("total"));
        } catch (SQLException ex) {
            Logger.getLogger(Sale.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }
}
