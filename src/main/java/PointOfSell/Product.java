/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package PointOfSell;

import java.util.ArrayList;

/**
 *
 * @author focus
 */
public class Product {
    private int id;
    private String name;
    private double price;


    public Product(int id, String name, double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + '}';
    }
    //ทำไว้ mockuk
    public static ArrayList<Product> getMockProductList(){
        String name;
        ArrayList<Product> list = new ArrayList<Product>();
        /*for(int i=0; i<10; i++){
            
            list.add(new Product(i+1, "Coffee "+ i+1, (i+1) *5));
        }*/
        list.add(new Product(0, "Confirm "+ "Order", (0)));
        list.add(new Product(1, "Latte "+ "(Hot)", (30)));
        list.add(new Product(2, "Latte "+ "(Iced)", (35)));
        list.add(new Product(3, "Latte "+ "(Freppe)", (40)));
        list.add(new Product(4, "Coacoa "+ "(Hot)", (30)));
        list.add(new Product(5, "Coacoa "+ "(Iced)", (35)));
        list.add(new Product(6, "Coacoa "+ "(Freppe)", (40)));
        list.add(new Product(7, "Apple soda "+ "(Iced)", (35)));
        list.add(new Product(8, "Lemon soda "+ "(Iced)", (35)));
        list.add(new Product(9, "Peach soda "+ "(Iced)", (35)));
        list.add(new Product(10, "Water "+ "(bottle)", (10)));
        list.add(new Product(11, "Coke "+ "(can)", (20)));
        list.add(new Product(12, "Ice "+ "(glass)", (5)));
        
        return list;
    }
    
}
