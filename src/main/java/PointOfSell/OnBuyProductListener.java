/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package PointOfSell;

/**
 *
 * @author focus
 */
public interface OnBuyProductListener {

    // เรียกตัวที่ subscribe
    public void buy(Product product, int amount);

}
