/*
 */
package Model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author INGK
 */
public class Orders {



    private int orders_Id;
    private int branch_id;
    private int orders_item_id;
    private String emp_id;
    private int cus_id;
    private double orders_total;
    private String orders_discount;
    private String orders_cash;
    private String orders_change;
    private int orders_queue;
    private String orders_date;
    private String orders_time;
    
    public Orders(int orders_Id, int branch_id, int orders_item_id, String emp_id,int cus_id, double orders_total, String orders_discount,String orders_cash, String orders_change,int orders_queue,String orders_date,String orders_time) {

        this.orders_Id = orders_Id;
        this.branch_id = branch_id;
        this.orders_item_id = orders_item_id;
        this.emp_id = emp_id;
        this.cus_id = cus_id;
        this.orders_total = orders_total;
        this.orders_discount = orders_discount;
        this.orders_cash = orders_cash;
        this.orders_change = orders_change;
        this.orders_queue = orders_queue;
        this.orders_date = orders_date;
        this.orders_time = orders_time;

    }
    public Orders( int branch_id, int orders_item_id, String emp_id,int cus_id, double orders_total, String orders_discount,String orders_cash, String orders_change,int orders_queue,String orders_date,String orders_time) {
        this.orders_Id = -1;
        this.branch_id = branch_id;
        this.orders_item_id = orders_item_id;
        this.emp_id = emp_id;
        this.cus_id = cus_id;
        this.orders_total = orders_total;
        this.orders_discount = orders_discount;
        this.orders_cash = orders_cash;
        this.orders_change = orders_change;
        this.orders_queue = orders_queue;
        this.orders_date = orders_date;
        this.orders_time = orders_time;

    }       

    public int getOrders_Id() {
        return orders_Id;
    }

    public void setOrders_Id(int orders_Id) {
        this.orders_Id = orders_Id;
    }

    public int getBranch_id() {
        return branch_id;
    }

    public void setBranch_id(int branch_id) {
        this.branch_id = branch_id;
    }

    public int getOrders_item_id() {
        return orders_item_id;
    }

    public void setOrders_item_id(int orders_item_id) {
        this.orders_item_id = orders_item_id;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public int getCus_id() {
        return cus_id;
    }

    public void setCus_id(int cus_id) {
        this.cus_id = cus_id;
    }

    public double getOrders_total() {
        return orders_total;
    }

    public void setOrders_total(double orders_total) {
        this.orders_total = orders_total;
    }

    public String getOrders_discount() {
        return orders_discount;
    }

    public void setOrders_discount(String orders_discount) {
        this.orders_discount = orders_discount;
    }

    public String getOrders_cash() {
        return orders_cash;
    }

    public void setOrders_cash(String orders_cash) {
        this.orders_cash = orders_cash;
    }

    public String getOrders_change() {
        return orders_change;
    }

    public void setOrders_change(String orders_change) {
        this.orders_change = orders_change;
    }

    public int getOrders_queue() {
        return orders_queue;
    }

    public void setOrders_queue(int orders_queue) {
        this.orders_queue = orders_queue;
    }

    public String getOrders_date() {
        return orders_date;
    }

    public void setOrders_date(String orders_date) {
        this.orders_date = orders_date;
    }

    public String getOrders_time() {
        return orders_time;
    }

    public void setOrders_time(String orders_time) {
        this.orders_time = orders_time;
    }

    
        public Orders(){
        this.orders_Id = -1;
        this.branch_id = branch_id;
        this.orders_item_id = orders_item_id;
        this.emp_id = emp_id;
        this.cus_id = cus_id;
        this.orders_total = orders_total;
        this.orders_discount = orders_discount;
        this.orders_cash = orders_cash;
        this.orders_change = orders_change;
        this.orders_queue = orders_queue;
        this.orders_date = orders_date;
        this.orders_time = orders_time;
    }   

    @Override
    public String toString() {
        return "Orders{" + "orders_Id=" + orders_Id + ", branch_id=" + branch_id + ", orders_item_id=" + orders_item_id + ", emp_id=" + emp_id + ", cus_id=" + cus_id + ", orders_total=" + orders_total + ", orders_discount=" + orders_discount + ", orders_cash=" + orders_cash + ", orders_change=" + orders_change + ", orders_queue=" + orders_queue + ", orders_date=" + orders_date + ", orders_time=" + orders_time + '}';
    }


     public static Orders fromRS(ResultSet rs) {
        Orders  order = new Orders();
        try {
            order.setOrders_Id(rs.getInt("orders_id"));
            order.setBranch_id(rs.getInt("branch_id"));
            order.setOrders_item_id(rs.getInt("orders_item_id"));
            order.setEmp_id(rs.getString("emp_id"));
            order.setCus_id(rs.getInt("cus_id"));
            order.setOrders_total(rs.getDouble("orders_total"));
            order.setOrders_discount(rs.getString("orders_discount"));
            order.setOrders_cash(rs.getString("orders_cash"));
            order.setOrders_change(rs.getString("orders_change"));
            order.setOrders_queue(rs.getInt("orders_queue"));
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyy-MM-dd");
            order.setOrders_date(rs.getString("orders_date"));
            SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");
            order.setOrders_time(rs.getString("orders_time"));
        } catch (SQLException ex) {
            Logger.getLogger(Orders.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return  order;
    }
}


