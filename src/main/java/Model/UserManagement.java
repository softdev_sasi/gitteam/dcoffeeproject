/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author USER
 */
public class UserManagement {

   
    
    private int id;
    private String username;
    private String password;
    private String date;
    private String timein;
    private String timeout;

    public UserManagement(int id,String username, String password,String date, String timein, String timeout) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.date = date;
        this.timein = timein;
        this.timeout = timeout;
    }
    
        public UserManagement(String username, String password, String date, String timein, String timeout) {
        this.id = -1;
        this.username = username;
        this.password = password;
        this.date = date;
        this.timein = timein;
        this.timeout = timeout;
    }

    public UserManagement() {
        this.id = -1;
        this.username = username;
        this.password = password;
        this.date = date;
        this.timein = timein;
        this.timeout = timeout;
    }
        

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
      public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTimein() {
        return timein;
    }

    public void setTimein(String timein) {
        this.timein = timein;
    }

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    @Override
    public String toString() {
        return "UserManagement{" + "id=" + id + ", username=" + username + ", password=" + password + ", date=" + date + ", timein=" + timein + ", timeout=" + timeout + '}';
    }
    
 public static UserManagement fromRS(ResultSet rs) {
       UserManagement user = new UserManagement();
        try {
            user.setId(rs.getInt("user_id"));
            user.setUsername(rs.getString("emp_username"));
            user.setPassword(rs.getString("emp_password"));
            user.setDate(rs.getString("atd_date"));
            user.setTimein(rs.getString("atd_time_in"));
            user.setTimeout(rs.getString("atd_time_out"));
        } catch (SQLException ex) {
            Logger.getLogger(UserManagement.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } 
        return user;
    }
        
    
    
}

