/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class Product {

    private int product;
    private String name;
    private int category;
    private int sweet;
    private int type;
    private double price;

    public Product(int product, String name, int category, int sweet, int type, double price) {
        this.product = product;
        this.name = name;
        this.category = category;
        this.sweet = sweet;
        this.type = type;
        this.price = price;
    }

    public Product( String name, int category, int sweet, int type, double price) {
        this.product = -1;
        this.name = name;
        this.category = category;
        this.sweet = sweet;
        this.type = type;
        this.price = price;
    }
    public Product() {
        this.product = -1;
        this.name =name;
        this.category = category;
        this.sweet = sweet;
        this.type = type;
        this.price = price;
    }
    

    public int getProduct() {
        return product;
    }

    public String getName() {
        return name;
    }

    public int getCategory() {
        return category;
    }

    public int getSweet() {
        return sweet;
    }

    public int getType() {
        return type;
    }

    public double getPrice() {
        return price;
    }

    public void setProduct(int product) {
        this.product = product;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public void setSweet(int sweet) {
        this.sweet = sweet;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" + "product=" + product + ", name=" + name + ", category=" + category + ", sweet=" + sweet + ", type=" + type + ", price=" + price + '}';
    }
    public static Product fromRS(ResultSet rs) {
        Product product = new Product();
        try {
            product.setProduct(rs.getInt("Product_id"));
            product.setName(rs.getString("Product_name"));
            product.setCategory(rs.getInt("category_id"));
            product.setSweet(rs.getInt("product_sweet_level"));
            product.setType(rs.getInt("product_type _id"));
            product.setPrice(rs.getDouble("product_price"));
            
        } catch (SQLException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return product;
    }
    
            
    

}
