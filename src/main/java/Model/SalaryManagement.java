/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class SalaryManagement {

    private int id;
    private int employeeid;
    private int attendenceid;
    private int totalworking;
    private String status;
    private String date;
    private int salary;

    public SalaryManagement(int id, int employeeid,int attendenceid, int totalworking, String status, String date, int salary) {
        this.id = id;
        this.employeeid = employeeid;
        this.attendenceid = attendenceid;
        this.totalworking = totalworking;
        this.status = status;
        this.date = date;
        this.salary = salary;
    }

    public SalaryManagement(int employeeid,int attendenceid, int totalworking, String status, String date, int salary) {
        this.id = -1;
        this.employeeid = employeeid;
        this.attendenceid = attendenceid;
        this.totalworking = totalworking;
        this.status = status;
        this.date = date;
        this.salary = salary;
    }

    private SalaryManagement() {
        this.id = -1;
        this.employeeid = employeeid;
        this.attendenceid = attendenceid;
        this.totalworking = totalworking;
        this.status = status;
        this.date = date;
        this.salary = salary;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmployeeid() {
        return employeeid;
    }

    public void setEmployeeid(int employeeid) {
        this.employeeid = employeeid;
    }

    public int getAttendenceid() {
        return attendenceid;
    }

    public void setAttendenceid(int attendenceid) {
        this.attendenceid = attendenceid;
    }

    public int getTotalworking() {
        return totalworking;
    }

    public void setTotalworking(int totalworking) {
        this.totalworking = totalworking;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "SalaryManagement{" + "id=" + id + ", employeeid=" + employeeid + ", attendenceid=" + attendenceid + ", totalworking=" + totalworking + ", status=" + status + ", date=" + date + ", salary=" + salary + '}';
    }
    
    
    public static SalaryManagement fromrs(ResultSet rs) {
        SalaryManagement slr = new SalaryManagement();
        try {
            slr.setId(rs.getInt("slr_report_id"));
            slr.setEmployeeid(rs.getInt("emp_id"));
            slr.setAttendenceid(rs.getInt("atd_time_id"));
            slr.setStatus(rs.getString("status_pay"));
            slr.setTotalworking(rs.getInt("atd_hours"));
            slr.setSalary(rs.getInt("emp_salary"));
            slr.setDate(rs.getString("date_to_pay"));
        } catch (SQLException ex) {
            Logger.getLogger(SalaryManagement.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return slr;
    }
}
