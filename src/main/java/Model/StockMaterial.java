/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author admin
 */
public class StockMaterial {

   
    private int id;
    private String name;
    private int minimum;
    private int remaining;
    private String unit;

    public StockMaterial(int id, String name, int minimum, int remaining, String unit) {
        this.id = id;
        this.name = name;
        this.minimum = minimum;
        this.remaining = remaining;
        this.unit = unit;
    }
    
    public StockMaterial(String name, int minimum, int remaining, String unit) {
        this.id = -1;
        this.name = name;
        this.minimum = minimum;
        this.remaining = remaining;
        this.unit = unit;
    }
    
    public StockMaterial() {
        this.id = -1;
        this.name = name;
        this.minimum = minimum;
        this.remaining = remaining;
        this.unit = unit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMinimum() {
        return minimum;
    }

    public void setMinimum(int minimum) {
        this.minimum = minimum;
    }

    public int getRemaining() {
        return remaining;
    }

    public void setRemaining(int remaining) {
        this.remaining = remaining;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "StockMaterial{" + "id=" + id + ", name=" + name + ", minimum=" + minimum + ", remaining=" + remaining + ", unit=" + unit + '}';
    }
    
     public static StockMaterial fromRS(ResultSet rs) {
         StockMaterial stockMtr = new StockMaterial();
         try {
            stockMtr.setId(rs.getInt("stock_mtr_id"));
            stockMtr.setName(rs.getString("stock_mtr_name"));
            stockMtr.setMinimum(rs.getInt("stock_mtr_minimum"));
            stockMtr.setRemaining(rs.getInt("stock_mtr_remaining"));
            stockMtr.setUnit(rs.getString("stock_mtr_unit"));
        } catch (SQLException ex) {
            Logger.getLogger(StockMaterial.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return stockMtr;
     }
}
