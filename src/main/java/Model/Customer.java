/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Thanya
 */
public class Customer {
    
    private int id;
    private String name;
    private String lastName;
    private String phone;
    private int point;
    private String date;

    public Customer(int id, String name, String lastName, String phone, int point, String date) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.phone = phone;
        this.point = point;
        this.date = date;
    }

    public Customer(String name, String lastName, String phone, int point, String date) {
        this.id = -1;
        this.name = name;
        this.lastName = lastName;
        this.phone = phone;
        this.point = point;
        this.date = date;
    }

    public Customer() {
        this.id = -1;
        this.name = name;
        this.lastName = lastName;
        this.phone = phone;
        this.point = point;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + id + ", name=" + name + ", lastName=" + lastName + ", phone=" + phone + ", point=" + point + ", date=" + date + '}';
    }
    
    public static Customer fromRs(ResultSet rs) {
        Customer cus = new Customer();
        try {
            cus.setId(rs.getInt("cus_id"));
            cus.setName(rs.getString("cus_firstname"));
            cus.setLastName(rs.getString("cus_lastname"));
            cus.setPhone(rs.getString("cus_phone"));
            cus.setPoint(rs.getInt("cus_point"));
            cus.setDate(rs.getString("cus_date_createmember"));
        } catch (SQLException ex) {
            Logger.getLogger(Customer.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return cus;
    }
}
