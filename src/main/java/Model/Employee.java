/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Thanya
 */
public class Employee {
    private int id;
    private int branchId;
    private String username;
    private String password;
    private String position;
    private String firstname;
    private String lastname;
    private int age;
    private String gender;
    private String phone;
    private String email;
    private String address;
    private int salary;

    public Employee(int id, int branchId, String username, String password, String position, String firstname, String lastname, int age, String gender, String phone, String email, String address, int salary) {
        this.id = id;
        this.branchId = branchId;
        this.username = username;
        this.password = password;
        this.position = position;
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
        this.gender = gender;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.salary = salary;
    }

    public Employee(int branchId, String username, String password, String position, String firstname, String lastname, int age, String gender, String phone, String email, String address, int salary) {
        this.id = -1;
        this.branchId = branchId;
        this.username = username;
        this.password = password;
        this.position = position;
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
        this.gender = gender;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.salary = salary;
    }

    public Employee() {
        this.id = -1;
        this.branchId = branchId;
        this.username = username;
        this.password = password;
        this.position = position;
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
        this.gender = gender;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", branchId=" + branchId + ", username=" + username + ", password=" + password + ", position=" + position + ", firstname=" + firstname + ", lastname=" + lastname + ", age=" + age + ", gender=" + gender + ", phone=" + phone + ", email=" + email + ", address=" + address + ", salary=" + salary + '}';
    }
    
    public static Employee fromRS(ResultSet rs) { 
        Employee emp = new Employee();
        try {
            emp.setId(rs.getInt("emp_id"));
            emp.setBranchId(rs.getInt("branch_id"));
            emp.setUsername(rs.getString("emp_username"));
            emp.setPassword(rs.getString("emp_password"));
            emp.setPosition(rs.getString("emp_position"));
            emp.setFirstname(rs.getString("emp_firstname"));
            emp.setLastname(rs.getString("emp_lastname"));
            emp.setAge(rs.getInt("emp_age"));
            emp.setGender(rs.getString("emp_gender"));
            emp.setPhone(rs.getString("emp_phone"));
            emp.setEmail(rs.getString("emp_email"));
            emp.setAddress(rs.getString("emp_address"));
            emp.setSalary(rs.getInt("emp_salary"));
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return emp;
    }
}
