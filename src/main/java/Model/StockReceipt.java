/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author admin
 */
public class StockReceipt {

    
    int id;
    String storeName;
    String name;
    int quantity;
    double totalPrice;

    public StockReceipt(int id, String storeName, String name, int quantity, double price) {
        this.id = id;
        this.storeName = storeName;
        this.name = name;
        this.quantity = quantity;
        this.totalPrice = price;
    }

    public StockReceipt(String storeName, String name, int quantity, double price) {
        this.id = -1;
        this.storeName = storeName;
        this.name = name;
        this.quantity = quantity;
        this.totalPrice = price;
    }

    public StockReceipt() {
        this.id = -1;
        this.storeName = storeName;
        this.name = name;
        this.quantity = quantity;
        this.totalPrice = totalPrice;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return totalPrice;
    }

    public void setPrice(double price) {
        this.totalPrice = price;
    }

    @Override
    public String toString() {
        return "StockReceipt{" + "id=" + id + ", storeName=" + storeName + ", name=" + name + ", quantity=" + quantity + ", price=" + totalPrice + '}';
    }
    
    public static StockReceipt fromRS(ResultSet rs) {
        StockReceipt stockRct = new StockReceipt();
         try {
            stockRct.setId(rs.getInt("receipt_item_id"));
            stockRct.setStoreName(rs.getString("receipt_store_name"));
            stockRct.setName(rs.getString("stock_mtr_name"));
            stockRct.setQuantity(rs.getInt("receipt_item_quantity"));
            stockRct.setPrice(rs.getDouble("receipt_item_total_price"));
        } catch (SQLException ex) {
            Logger.getLogger(StockReceipt.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return stockRct;
    }
}
